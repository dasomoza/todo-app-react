import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import AddButton from "../add-button/AddButton";
import "./AddNewTask.css";

function AddNewTask(props) {

    const [text, setText] = useState("");

    function handleAddNewTask() {
        if (text.trim() !== "") {
            props.addNewTask({
                id: uuidv4(),
                name: text.trim(),
                finished: false
            });
        }
        setText("");
    }

    return <div className="container">
        <input
            id="input-add-task"
            value={text}
            onChange={(event) => setText(event.target.value)}
            placeholder="Add your new task" className="input"
            onKeyPress={(event) => {
                if(event.key === "Enter") {
                    handleAddNewTask();
                }
            }}
        />
        <AddButton onClick={handleAddNewTask} />
    </div>
}

export default AddNewTask;