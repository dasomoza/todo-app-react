import { fireEvent, render, screen } from "@testing-library/react";
import AddNewTask from "./AddNewTask";
import { v4 as uuidv4 } from 'uuid';
jest.mock('uuid');

describe("Componente AddNewTask", () => {
    test("debe llamarse a addnewTask cuando le damos al boton de añadir tarea", () => {
        
        uuidv4.mockImplementation(() => 'testid');
        const mockedAddNewTaskFunction = jest.fn();

        render(<AddNewTask addNewTask={mockedAddNewTaskFunction} />);

        const inputDOM = screen.getByTestId("input-add-task");

        fireEvent.change(inputDOM, { target: { value: 'nueva tarea' } });

        const buttonDOM = screen.getByTestId("add-button");

        fireEvent.click(buttonDOM);

        expect(mockedAddNewTaskFunction).toHaveBeenCalledWith({
            id: uuidv4(),
            name: 'nueva tarea',
            finished: false
        });
    });

    test("no introducir una tarea SI no he tecleado nada en el campo de texto", () => {
        
        uuidv4.mockImplementation(() => 'testid');
        const mockedAddNewTaskFunction = jest.fn();

        render(<AddNewTask addNewTask={mockedAddNewTaskFunction} />);

        const buttonDOM = screen.getByTestId("add-button");

        fireEvent.click(buttonDOM);

        expect(mockedAddNewTaskFunction).not.toHaveBeenCalled();
    });


    test("no introducir una tarea SI solo tecleo espacios nada en el campo de texto", () => {
        
        uuidv4.mockImplementation(() => 'testid');
        const mockedAddNewTaskFunction = jest.fn();

        render(<AddNewTask addNewTask={mockedAddNewTaskFunction} />);

        const inputDOM = screen.getByTestId("input-add-task");

        fireEvent.change(inputDOM, { target: { value: '       ' } });

        const buttonDOM = screen.getByTestId("add-button");

        fireEvent.click(buttonDOM);

        expect(mockedAddNewTaskFunction).not.toHaveBeenCalled();
    });

    test("debe eliminarse los espacios finales en el nombre de la tarea", () => {
        
        uuidv4.mockImplementation(() => 'testid');
        const mockedAddNewTaskFunction = jest.fn();

        render(<AddNewTask addNewTask={mockedAddNewTaskFunction} />);

        const inputDOM = screen.getByTestId("input-add-task");

        fireEvent.change(inputDOM, { target: { value: 'nueva tarea        ' } });

        const buttonDOM = screen.getByTestId("add-button");

        fireEvent.click(buttonDOM);

        expect(mockedAddNewTaskFunction).toHaveBeenCalledWith({
            id: uuidv4(),
            name: 'nueva tarea',
            finished: false
        });
    });
    
});