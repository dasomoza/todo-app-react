import { IoIosAddCircleOutline } from "react-icons/io";
import "./AddButton.css"

function AddButton({ onClick }) {
    return <button id="add-button" onClick={onClick} className={"add-btn"} >
        <IoIosAddCircleOutline />
    </button>;
}

export default AddButton;
