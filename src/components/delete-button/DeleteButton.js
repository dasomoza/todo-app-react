import { IoIosTrash } from "react-icons/io";
import "./DeleteButton.css";

function DeleteButton(props) {

    return <button className="btn-delete" onClick={props.onClick}>
        <IoIosTrash />
    </button>
}

export default DeleteButton;