import ClearAllButton from "../clear-all-button/ClearAllButton";

function Footer(props) {

    const pendingTasks = props.tasks.filter(task => !task.finished).length;

    return <div>{`you have ${pendingTasks} pending tasks`}
        <ClearAllButton onClick={props.clearAll} />
    </div>
}


export default Footer;