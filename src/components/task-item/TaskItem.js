import React from "react";
import DeleteButton from "../delete-button/DeleteButton";
import "./TaskItem.css"

class TaskItem extends React.Component {

    render() {

        return <div className="container-task-item" style={{
            textDecoration: this.props.task.finished ? "line-through" : "initial"
        }} >
            {this.props.task.name}
            <DeleteButton onClick={() => this.props.removeTask(this.props.task.id)}/>
        </div>
    }
}

export default TaskItem;