import TaskItem from "../task-item/TaskItem";

function TaskList({ tasks, removeTask }) {
    
    return tasks.map(function(task) {
        return <TaskItem task={task} key={task.id} removeTask={removeTask} />
    })
}

export default TaskList;