import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import './App.css';
import AddNewTask from './components/add-new-task/AddNewTask';
import Footer from './components/footer/Footer';
import TaskList from './components/task-list/TaskList';
import Title from './components/title/Title';

const INITIAL_TASKS = [
  {
    id: uuidv4(),
    name: "Learn JavaScript",
    finished: false
  },
  {
    id: uuidv4(),
    name: "Learn CSS",
    finished: true
  },
  {
    id: uuidv4(),
    name: "Learn React",
    finished: false
  }
];

function App() {
   const [tasks, setTasks] = useState(INITIAL_TASKS);

   function addNewTask(newTask) {
    setTasks([...tasks, newTask]);
   }

   function removeTask(id) {
     const filterTasks = tasks.filter( task => task.id !== id);
     setTasks(filterTasks);
   }

   console.log(tasks);
  return (
    <div className="App">
      <Title title="Todo App" />
      <AddNewTask addNewTask={addNewTask} />
      <TaskList tasks={tasks} removeTask={removeTask} />
      <Footer tasks={tasks} clearAll={() => setTasks([])} />
    </div>
  );
}

export default App;
